package com.ibarbu.marketingapp.channels

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ibarbu.marketingapp.databinding.ItemChannelBinding

class ChannelsAdapter(
    private val channels: List<String>,
    private val onClickListener: View.OnClickListener? = null
) : RecyclerView.Adapter<ChannelViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChannelViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = ItemChannelBinding.inflate(layoutInflater, parent, false)

        onClickListener?.let {
            itemBinding.itemContainer.setOnClickListener { view ->
                it.onClick(view)
            }
        }
        return ChannelViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        return channels.size
    }

    override fun onBindViewHolder(holder: ChannelViewHolder, position: Int) {
        with(holder.binding) {
            name = channels[position]
            itemContainer.tag = channels[position]
        }
    }
}

class ChannelViewHolder(val binding: ItemChannelBinding) : RecyclerView.ViewHolder(binding.root)