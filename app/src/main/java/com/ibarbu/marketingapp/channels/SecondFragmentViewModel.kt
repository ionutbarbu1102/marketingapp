package com.ibarbu.marketingapp.channels

import androidx.lifecycle.ViewModel
import com.ibarbu.marketingapp.data.DataRepository

class SecondFragmentViewModel: ViewModel() {

    fun isAtLeastOneCampaignSaved(): Boolean = DataRepository.getSelectedCampaigns().isNotEmpty()

}