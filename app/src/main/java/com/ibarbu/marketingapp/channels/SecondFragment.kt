package com.ibarbu.marketingapp.channels

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ibarbu.marketingapp.R
import com.ibarbu.marketingapp.data.DataRepository
import com.ibarbu.marketingapp.databinding.FragmentSecondBinding

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class SecondFragment : Fragment() {

    private val secondViewModel: SecondFragmentViewModel by viewModels()
    private var _binding: FragmentSecondBinding? = null
    private val binding get() = _binding!!

    private val onClickListener = View.OnClickListener { view ->
        val bundle = bundleOf("channel" to view.tag as String)
        findNavController().navigate(R.id.action_SecondFragment_to_FeatureFragment, bundle)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        with(binding.channelsRV) {
            adapter = ChannelsAdapter(DataRepository.getChannels(), onClickListener)
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonPrevious.setOnClickListener {
            findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)
        }
        binding.buttonReview.setOnClickListener {
            if (secondViewModel.isAtLeastOneCampaignSaved()) {
                findNavController().navigate(R.id.action_SecondFragment_to_PreviewFragment)
            } else {
                Toast.makeText(requireContext(), R.string.select_at_least_one, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}