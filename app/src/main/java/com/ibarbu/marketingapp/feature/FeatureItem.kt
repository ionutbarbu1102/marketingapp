package com.ibarbu.marketingapp.feature

data class FeatureItem (
        val description: String,
        var isSelected: Boolean = false
)