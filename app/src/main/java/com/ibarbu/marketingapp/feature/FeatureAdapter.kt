package com.ibarbu.marketingapp.feature

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ibarbu.marketingapp.databinding.ItemFeatureBinding

class FeatureAdapter(private val viewModel: FeatureViewModel) :
    RecyclerView.Adapter<FeatureViewHolder>() {
    private val mItems = mutableListOf<FeatureItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeatureViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = ItemFeatureBinding.inflate(layoutInflater, parent, false)

        itemBinding.itemContainer.setOnClickListener { view ->
            val clickedPosition = (view.tag as Int)
            mItems[clickedPosition].isSelected = !mItems[clickedPosition].isSelected
            viewModel.saveSelectedCampaign(mItems[clickedPosition].description)
            mItems.filterIndexed { index, item -> index != clickedPosition && item.isSelected }
                .forEach { item -> item.isSelected = false }
            notifyDataSetChanged()
        }
        return FeatureViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    override fun onBindViewHolder(holder: FeatureViewHolder, position: Int) {
        with(holder.binding) {
            features = mItems[position].description
            isChecked = mItems[position].isSelected
            itemContainer.tag = position
        }
    }

    fun updateItems(items: List<FeatureItem>) {
        mItems.clear()
        mItems.addAll(items)
        mItems.filter { it.description == viewModel.getSelectedCampaign() }
            .forEach { it.isSelected = true }
        notifyDataSetChanged()
    }
}

class FeatureViewHolder(val binding: ItemFeatureBinding) : RecyclerView.ViewHolder(binding.root)
