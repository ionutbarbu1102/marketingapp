package com.ibarbu.marketingapp.feature

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.ibarbu.marketingapp.R
import com.ibarbu.marketingapp.databinding.FragmentFeatureBinding
import com.ibarbu.marketingapp.targeting.ERROR
import com.ibarbu.marketingapp.targeting.LOADING
import com.ibarbu.marketingapp.targeting.SUCCESS

class FeatureFragment : Fragment() {

    private val featureViewModel: FeatureViewModel by viewModels()
    private var _binding: FragmentFeatureBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFeatureBinding.inflate(inflater, container, false)
        val channel = arguments?.getString("channel", "") ?: ""
        with(binding.featuresRV) {
            adapter = FeatureAdapter(featureViewModel)
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            featureViewModel.setChannel(channel).observe(viewLifecycleOwner, {
                if (it.keys.contains(channel)) {
                    val featureItems = it[channel]?.map {
                        FeatureItem(it)
                    }
                    (adapter as FeatureAdapter).updateItems(featureItems ?: emptyList())
                }
            })
        }

        featureViewModel.requestStatus(channel).observe(viewLifecycleOwner, { status ->
            when (status) {
                SUCCESS -> binding.indeterminateBar.visibility = View.GONE
                ERROR -> {
                    Snackbar
                        .make(binding.root, getString(R.string.connection_error), Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry)) { featureViewModel.startGettingChannel() }
                        .show()
                    binding.indeterminateBar.visibility = View.GONE
                }
                LOADING -> binding.indeterminateBar.visibility = View.VISIBLE
            }
        })

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonPrev.setOnClickListener {
            findNavController().navigate(R.id.action_FeatureFragment_to_SecondFragment)
        }

        binding.buttonReset.setOnClickListener {
            featureViewModel.reset()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}