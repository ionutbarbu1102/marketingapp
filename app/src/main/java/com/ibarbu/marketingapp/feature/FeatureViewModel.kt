package com.ibarbu.marketingapp.feature

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.ibarbu.marketingapp.data.DataRepository
import com.ibarbu.marketingapp.data.Resource
import com.ibarbu.marketingapp.targeting.ERROR
import com.ibarbu.marketingapp.targeting.LOADING
import com.ibarbu.marketingapp.targeting.SUCCESS

class FeatureViewModel : ViewModel() {

    private var currentChannel = ""

    fun setChannel(channel: String): LiveData<Map<String, List<String>>> = Transformations.map(DataRepository.getChannelsFor(channel)){
        currentChannel = channel
        when(it) {
            is Resource.Success -> it.data
            is Resource.Loading -> it.data ?: emptyMap()
            else -> emptyMap()
        }

    }

    fun requestStatus(channel: String): LiveData<Int> = Transformations.map(DataRepository.getChannelsFor(channel)) {
        when(it) {
            is Resource.Error -> ERROR
            is Resource.Loading -> LOADING
            else -> SUCCESS
        }
    }

    fun saveSelectedCampaign(campaign: String) {
        DataRepository.updateSelectedCampaignFor(currentChannel, campaign)
    }

    fun reset() {
        DataRepository.removeCampaignFor(currentChannel)
    }

    fun getSelectedCampaign(): String = DataRepository.getSelectedCampaignFor(currentChannel)

    fun startGettingChannel() {
        DataRepository.startGettingChannelsFor(currentChannel)
    }
}