package com.ibarbu.marketingapp.data

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers

interface MarketingApi {

    @Headers("secret-key:\$2b\$10\$smL2EhaJMY2sF7H31QaAHuvYRAErKEUkqDL0Meq2nzMSdo1q8bu9u")
    @GET("5f9073e27243cd7e8251f326")
    fun getTargetingSpecifics(): Call<ResponseBody>

    @Headers("secret-key:\$2b\$10\$smL2EhaJMY2sF7H31QaAHuvYRAErKEUkqDL0Meq2nzMSdo1q8bu9u")
    @GET("5f9740ad076e516c36fbc169")
    fun getFacebookChannels(): Call<ResponseBody>

    @Headers("secret-key:\$2b\$10\$smL2EhaJMY2sF7H31QaAHuvYRAErKEUkqDL0Meq2nzMSdo1q8bu9u")
    @GET("5f9740d3076e516c36fbc178")
    fun getLinkedinChannels(): Call<ResponseBody>

    @Headers("secret-key:\$2b\$10\$smL2EhaJMY2sF7H31QaAHuvYRAErKEUkqDL0Meq2nzMSdo1q8bu9u")
    @GET("5f9740ea2923e61aff4e0927")
    fun getTwitterChannels(): Call<ResponseBody>

    @Headers("secret-key:\$2b\$10\$smL2EhaJMY2sF7H31QaAHuvYRAErKEUkqDL0Meq2nzMSdo1q8bu9u")
    @GET("5f9740fc30aaa01ce61958af")
    fun getInstagramChannels(): Call<ResponseBody>

    @Headers("secret-key:\$2b\$10\$smL2EhaJMY2sF7H31QaAHuvYRAErKEUkqDL0Meq2nzMSdo1q8bu9u")
    @GET("5f97410e30aaa01ce61958b6")
    fun getGoogleAdWordsChannels(): Call<ResponseBody>

    @Headers("secret-key:\$2b\$10\$smL2EhaJMY2sF7H31QaAHuvYRAErKEUkqDL0Meq2nzMSdo1q8bu9u")
    @GET("5f974125076e516c36fbc195")
    fun getSEOSChannels(): Call<ResponseBody>
}