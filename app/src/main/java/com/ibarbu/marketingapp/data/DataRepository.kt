package com.ibarbu.marketingapp.data

import androidx.annotation.StringDef
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ibarbu.marketingapp.data.CHANNELS.Companion.FACEBOOK
import com.ibarbu.marketingapp.data.CHANNELS.Companion.GOOGLE_AD_WORDS
import com.ibarbu.marketingapp.data.CHANNELS.Companion.INSTAGRAM
import com.ibarbu.marketingapp.data.CHANNELS.Companion.LINKEDIN
import com.ibarbu.marketingapp.data.CHANNELS.Companion.SEO
import com.ibarbu.marketingapp.data.CHANNELS.Companion.TWITTER
import okhttp3.ResponseBody
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object DataRepository {

    private const val BASE_URL = "https://api.jsonbin.io/b/"

    private val marketingApi: MarketingApi

    private val mTargetingSpecificsLiveData = MutableLiveData<Resource<Map<String, List<String>>>>()
    private val mChannelsLiveData = MutableLiveData<Resource<Map<String, List<String>>>>()

    private val mSelectedTargetingSpecifics = mutableSetOf<String>()

    private val mSelectedCampaigns = mutableMapOf<String, String>()

    private var lastGettingOfTargetingSpecifics = 0L

    init {
        val retrofitClient = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        marketingApi = retrofitClient.create(MarketingApi::class.java)
    }

    fun getTargetingSpecifics(): LiveData<Resource<Map<String, List<String>>>> {
        if (isEnoughTimeSinceGettingLastTimeTargetingSpecifics()) {
            startGettingTargetingSpecifics()
        }
        return mTargetingSpecificsLiveData
    }

    fun startGettingTargetingSpecifics() {
        lastGettingOfTargetingSpecifics = System.currentTimeMillis()
        mTargetingSpecificsLiveData.value = Resource.Loading(emptyMap())
        marketingApi.getTargetingSpecifics().enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                mTargetingSpecificsLiveData.value =
                    Resource.Error("No internet connection", emptyMap())
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                val jsonArray = JSONArray(response.body()!!.string())

                val targetingSpecificsMap = mutableMapOf<String, List<String>>()

                for (i in 0 until jsonArray.length()) {
                    val key = jsonArray.getJSONObject(i).names() ?: continue
                    val channels = jsonArray.getJSONObject(i).getJSONArray(key.getString(0))
                    val channelsList = mutableListOf<String>()
                    for (j in 0 until channels.length()) {
                        channelsList.add(channels.getString(j))
                    }
                    targetingSpecificsMap.put(key.getString(0), channelsList)

                }
                mTargetingSpecificsLiveData.value = Resource.Success(targetingSpecificsMap)
            }
        })
    }

    fun getChannelsFor(channel: String): LiveData<Resource<Map<String, List<String>>>> {
        startGettingChannelsFor(channel)
        return mChannelsLiveData
    }

    fun startGettingChannelsFor(channel: String) {
        val oldChannels = mChannelsLiveData.value?.data ?: emptyMap()
        mChannelsLiveData.value = Resource.Loading(emptyMap())
        if (oldChannels.containsKey(channel)) {
            mChannelsLiveData.value = Resource.Success(oldChannels)
            return
        }
        val responseBody = when (channel) {
            FACEBOOK -> marketingApi.getFacebookChannels()
            LINKEDIN -> marketingApi.getLinkedinChannels()
            TWITTER -> marketingApi.getTwitterChannels()
            INSTAGRAM -> marketingApi.getInstagramChannels()
            GOOGLE_AD_WORDS -> marketingApi.getGoogleAdWordsChannels()
            SEO -> marketingApi.getSEOSChannels()
            else -> throw IllegalArgumentException("Wrong channel")
        }
        responseBody.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                mChannelsLiveData.value = Resource.Error("No internet connection", emptyMap())
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                val jsonArray = JSONArray(response.body()!!.string())

                val channelsMap = mChannelsLiveData.value?.data?.toMutableMap() ?: mutableMapOf()
                val featureList = mutableListOf<String>()
                for (i in 0 until jsonArray.length()) {
                    val featureArray = jsonArray.getJSONArray(i)
                    var featureString = ""
                    for (j in 0 until featureArray.length()) {
                        featureString += featureArray.getString(j)
                        if (j < featureArray.length() - 1) {
                            featureString += "\n"
                        }
                    }
                    featureList.add(featureString)
                }
                channelsMap[channel] = featureList
                mChannelsLiveData.value = Resource.Success(channelsMap)
            }
        })
    }

    fun updateSelectedItems(name: String, isChecked: Boolean) {
        if (isChecked) {
            mSelectedTargetingSpecifics.add(name)
        } else {
            mSelectedTargetingSpecifics.remove(name)
        }
    }

    fun getSelectedItems(): List<String> = mSelectedTargetingSpecifics.toList()

    fun getChannels(): List<String> {
        val channelSet = mutableSetOf<String>()
        mSelectedTargetingSpecifics.forEach { selectedTargetingItem ->
            channelSet.addAll(
                (mTargetingSpecificsLiveData.value?.data?.get(selectedTargetingItem)
                    ?: error("Invalid channels")).toSet()
            )

        }
        return channelSet.toList()

    }

    fun clearSelectedTargetings() {
        mSelectedTargetingSpecifics.clear()
    }

    fun updateSelectedCampaignFor(channel: String, campaign: String) {
        mSelectedCampaigns[channel] = campaign
    }

    fun removeCampaignFor(channel: String) {
        mSelectedCampaigns.remove(channel)
        val channels = mChannelsLiveData.value ?: return
        mChannelsLiveData.value = channels
    }

    fun getSelectedCampaignFor(channel: String): String {
        return mSelectedCampaigns[channel] ?: ""
    }

    fun getSelectedCampaigns(): List<String> {
        return mSelectedCampaigns.map {
            it.key + "\n" + it.value
        }
    }

    private fun isEnoughTimeSinceGettingLastTimeTargetingSpecifics() =
        lastGettingOfTargetingSpecifics == 0L
                || System.currentTimeMillis() - lastGettingOfTargetingSpecifics >=
                TimeUnit.MINUTES.toMillis(2)

}

@Retention(AnnotationRetention.SOURCE)
@StringDef(FACEBOOK, LINKEDIN, TWITTER, INSTAGRAM, GOOGLE_AD_WORDS, SEO)
annotation class CHANNELS {
    companion object {
        const val FACEBOOK = "Facebook"
        const val LINKEDIN = "Linkedin"
        const val TWITTER = "Twitter"
        const val INSTAGRAM = "Instagram"
        const val GOOGLE_AD_WORDS = "Google AdWords"
        const val SEO = "SEO"
    }
}

// A generic class that contains data and status about loading this data.
sealed class Resource<T>(
    val data: T? = null,
    val message: String? = null
) {
    class Success<T>(data: T) : Resource<T>(data)
    class Loading<T>(data: T? = null) : Resource<T>(data)
    class Error<T>(message: String, data: T? = null) : Resource<T>(data, message)
}