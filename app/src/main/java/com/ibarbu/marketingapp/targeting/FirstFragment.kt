package com.ibarbu.marketingapp.targeting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.ibarbu.marketingapp.R
import com.ibarbu.marketingapp.databinding.FragmentFirstBinding

class FirstFragment : Fragment() {

    private val mTargetingViewModel: TargetingViewModel by viewModels()
    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFirstBinding.inflate(inflater, container, false)

        with(binding.targetingRV) {
            adapter = TargetingAdapter(mTargetingViewModel)
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        }
        mTargetingViewModel.targetingItems.observe(viewLifecycleOwner, {
            (binding.targetingRV.adapter as TargetingAdapter).updateItems(it)
        })
        mTargetingViewModel.requestStatus.observe(viewLifecycleOwner, { status ->
            when (status) {
                SUCCESS -> binding.indeterminateBar.visibility = View.GONE
                ERROR -> {
                    Snackbar
                        .make(binding.root, getString(R.string.connection_error), Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry)) { mTargetingViewModel.startGettingTargetingSpecifics() }
                        .show()
                    binding.indeterminateBar.visibility = View.GONE
                }
                LOADING -> binding.indeterminateBar.visibility = View.VISIBLE
            }
        })
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonFirst.setOnClickListener {
            if (mTargetingViewModel.canGoToNext()) {
                findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
            } else {
                Toast.makeText(
                    requireContext(),
                    getString(R.string.select_one_option_at_least),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}