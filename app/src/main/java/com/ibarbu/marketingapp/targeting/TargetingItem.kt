package com.ibarbu.marketingapp.targeting

data class TargetingItem (
    val name: String,
    var isChecked: Boolean = false
)