package com.ibarbu.marketingapp.targeting

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.ibarbu.marketingapp.data.DataRepository
import com.ibarbu.marketingapp.data.Resource

class TargetingViewModel : ViewModel() {

    val requestStatus = Transformations.map(DataRepository.getTargetingSpecifics()) {
        when(it) {
            is Resource.Error -> ERROR
            is Resource.Loading -> LOADING
            else -> SUCCESS
        }
    }

    val targetingItems: LiveData<List<TargetingItem>> =
        Transformations.map(DataRepository.getTargetingSpecifics()) {
            val selectedItems = DataRepository.getSelectedItems()
            val mappedItems = it?.data?.keys?.map { itemName ->
                val isSelected = selectedItems.contains(itemName)
                TargetingItem(itemName, isSelected)
            }
            mappedItems ?: emptyList()
        }

    fun selectOption(name: String, isChecked: Boolean) {
        DataRepository.updateSelectedItems(name, isChecked)
    }

    fun canGoToNext(): Boolean {
        return DataRepository.getSelectedItems().isNotEmpty()
    }

    fun startGettingTargetingSpecifics() {
        DataRepository.startGettingTargetingSpecifics()
    }
}

const val ERROR = 0
const val LOADING = 1
const val SUCCESS = 2