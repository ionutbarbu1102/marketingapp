package com.ibarbu.marketingapp.targeting

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ibarbu.marketingapp.databinding.ItemTargetingBinding

class TargetingAdapter(private val viewModel: TargetingViewModel) :
    RecyclerView.Adapter<TargetingItemViewHolder>() {

    private val mItems = mutableListOf<TargetingItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TargetingItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = ItemTargetingBinding.inflate(layoutInflater, parent, false)

        itemBinding.itemContainer.setOnClickListener { view ->
            val clickedPosition = (view.tag as Int)
            val oldValue = mItems[clickedPosition].isChecked
            mItems[clickedPosition].isChecked = !oldValue
            viewModel.selectOption(mItems[clickedPosition].name, !oldValue)
            notifyItemChanged(clickedPosition)
        }
        return TargetingItemViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    override fun onBindViewHolder(holder: TargetingItemViewHolder, position: Int) {
        with(holder.binding) {
            name = mItems[position].name
            isChecked = mItems[position].isChecked
            itemContainer.tag = position
        }
    }

    fun updateItems(itemList: List<TargetingItem>) {
        mItems.clear()
        mItems.addAll(itemList)
        notifyDataSetChanged()
    }
}

class TargetingItemViewHolder(var binding: ItemTargetingBinding) :
    RecyclerView.ViewHolder(binding.root)