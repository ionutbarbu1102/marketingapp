package com.ibarbu.marketingapp

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.ibarbu.marketingapp.data.DataRepository

class MainActivity : AppCompatActivity() {

    private var pressOneMoreTimeToExit = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        if (pressOneMoreTimeToExit) {
            super.onBackPressed()
            DataRepository.clearSelectedTargetings()
            finish()
            return
        }

        Toast.makeText(this, getString(R.string.press_back_again), Toast.LENGTH_SHORT).show()
        pressOneMoreTimeToExit = true
        Handler(Looper.getMainLooper()).postDelayed({ pressOneMoreTimeToExit = false}, 2000)
    }
}