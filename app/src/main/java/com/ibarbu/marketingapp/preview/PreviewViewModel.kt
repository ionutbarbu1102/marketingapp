package com.ibarbu.marketingapp.preview

import androidx.lifecycle.ViewModel
import com.ibarbu.marketingapp.data.DataRepository

class PreviewViewModel: ViewModel() {

    fun getCampaignsAsString(): String {
        return DataRepository.getSelectedCampaigns().reduce{acc, item -> acc + "\n" + item + "\n\n"}
    }
}