package com.ibarbu.marketingapp.preview

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ibarbu.marketingapp.channels.ChannelsAdapter
import com.ibarbu.marketingapp.data.DataRepository
import com.ibarbu.marketingapp.databinding.FragmentPreviewBinding

class PreviewFragment : Fragment() {

    private val previewViewModel: PreviewViewModel by viewModels()
    private var _binding: FragmentPreviewBinding? = null
    private val binding get() = _binding!!
    
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPreviewBinding.inflate(inflater, container, false)
        with(binding.previewRV) {
            adapter = ChannelsAdapter(DataRepository.getSelectedCampaigns())
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonSend.setOnClickListener {
            sendEmail(previewViewModel.getCampaignsAsString())
        }

        binding.buttonBack.setOnClickListener {
            findNavController().navigateUp()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }



    private fun sendEmail(body: String) {
        val intent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("mailto:")
            putExtra(Intent.EXTRA_SUBJECT, "Marketing campaigns")
            putExtra(Intent.EXTRA_TEXT, body)
            putExtra(Intent.EXTRA_EMAIL, arrayOf("bogus@bogus.com"))
        }
        startActivity(Intent.createChooser(intent, "Send campaigns"))
    }
}